import { App as Application } from "vue";
import Weather from "./Weather.vue";

import { registerComponent } from "@/utils/plugins/index";

const Plugin = {
  install(vue: Application) {
    registerComponent(vue, Weather);
  }
};

// use(Plugin);

export default Plugin;

export { Weather };
